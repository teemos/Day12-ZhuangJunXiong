import { useDispatch } from "react-redux";
import { removeTodo } from "./todoSlice";
import { changeTodoDone } from "./todoSlice";

const Item = (props) => {

    const {todo} = props

    const dispatch = useDispatch();
    
    const deleteTodo=()=>{
        dispatch(removeTodo(todo.id));
    }
    const changeTodoDoneState=(e)=>{
        dispatch(changeTodoDone(todo.id));
        
    }

    return (
        <div className="todoItem">
            <span onClick={changeTodoDoneState} style={{textDecoration:todo.done ? 'line-through' : 'none'}}>{todo.title}</span>
            <button onClick={deleteTodo}>X</button>
        </div>
    )
}
export default Item;
