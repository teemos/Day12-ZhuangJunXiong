import { createSlice } from "@reduxjs/toolkit";

const todoSlice = createSlice({
    name: 'todo',
    initialState: {
        todoList: [],
    },
    reducers: {
        addTodo: (state, action) => {
            const todo = action.payload;
            state.todoList = [...state.todoList, todo]
        },
        removeTodo: (state, action) => {
            const id = action.payload;
            state.todoList = state.todoList.filter((todo) => todo.id !== id);
        },
        changeTodoDone: (state, action) => {
            const id = action.payload;
            state.todoList.forEach(todo=>{
                if (todo.id === id) {
                    todo.done=!todo.done;
                }
            })
            
        },
    },
});

export default todoSlice.reducer;
export const { addTodo, removeTodo, changeTodoDone } = todoSlice.actions;