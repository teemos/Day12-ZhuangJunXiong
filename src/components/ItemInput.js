import { useDispatch } from "react-redux";
import { useState } from "react";
import { addTodo } from "./todoSlice";
import { nanoid } from "nanoid";

const ItemInput = (props) => {
    // const {addTodo} = props
    // const [text,setText]=useState('');
    // const handleInputChange = (event) => {
    //     const value = event.target.value;
    //     setText(value);
    // };
    // const add = () => {
    //     addTodo(text)
    //     setText ('')
    // }
    // return (
    //     <div>
    //         <input type="text" value={text} onChange={handleInputChange}/>
    //         <button onClick={add}>add</button>
    //     </div>
    // )
    const [title, setTitle] = useState('');
    const handleInputChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    };
    const dispatch = useDispatch();
    const handleChange = () => {
        if(title===''){
            return;
        }
        const newTodo = { id: nanoid(), title: title, done: false };
        dispatch(addTodo(newTodo));
        setTitle('');
    }
    return (
        <div className="todoInput">
            <input type="text" value={title} onChange={handleInputChange} />
            <button onClick={handleChange}>add</button>
        </div>
    )
}
export default ItemInput;