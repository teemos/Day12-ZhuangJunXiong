import Item from "./Item";
import { useSelector } from "react-redux";

const ItemList = () => {
    // const {todoList} = props
    const todoList = useSelector(state => state.todo.todoList);

    return (
        <div className="todoList">
            {todoList.map((todo, index) => {
                return <Item todo={todo} key={todo.id} />
            })}
        </div>
    )
}

export default ItemList;